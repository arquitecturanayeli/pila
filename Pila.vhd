library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity Pila is 
GENERIC( 
	addrs : INTEGER := 4;
	lit : INTEGER := 16);
Port(
	D : in std_logic_vector (lit-1 downto 0);
	wpc : in std_logic;
	dw  : in std_logic;
	up  : in std_logic;
	clk : in std_logic;
	clr : in std_logic;
	Q : out std_logic_vector (lit-1 downto 0)
	);
end Pila;

architecture Behavioral of Pila is
type ARREGLO is Array (0 to ((2**addrs)-1)) of std_logic_vector(lit-1 downto 0);
signal PC: arreglo;
signal aux_sp: INTEGER RANGE 0 TO ((2**addrs)-1);
begin
	process(clk, clr)
	variable sp : INTEGER RANGE 0 to ((2**addrs)-1);
	begin
		if(clr = '1')then
			sp := 0;
			PC <= (others => (others => '0'));
		elsif  (rising_edge(clk))then
			if(dw = '1' and up = '0' and wpc = '0')then -- RET
				sp := sp-1;
				PC(sp) <= PC(sp)+1;
			elsif( up = '1' and wpc = '1')then--call
				sp := sp + 1;
				PC(sp) <= D;
			elsif(wpc = '1')then-- B (salto)
				PC(sp) <= D;
			else
				PC(sp) <= PC(sp)+1;
			end if;
		
		end if;

		aux_sp <= sp;
	end process;
	Q <= PC(aux_sp);
end Behavioral;

